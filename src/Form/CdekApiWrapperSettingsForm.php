<?php

namespace Drupal\cdek_api_wrapper\Form;

use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;

class CdekApiWrapperSettingsForm extends ConfigFormBase {

  /**
   * {@inheritdoc}
   */
  public function getFormId(): string {
    return 'cdek_api_wrapper_settings_form';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames(): array {
    return ['cdek_api_wrapper.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {
    $form['cdek_api_mode'] = [
      '#type' => 'radios',
      '#title' => $this->t('Mode'),
      '#options' => [
        'test' => $this->t('Test'),
        'live' => $this->t('Live'),
      ],
      '#default_value' => $this->config('cdek_api_wrapper.settings')->get('cdek_api_mode'),
    ];

    $form['cdek_api_login'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDEK API account login'),
      '#default_value' => $this->config('cdek_api_wrapper.settings')->get('cdek_api_login'),
      '#states' => [
        'visible' => [
          ':input[name="cdek_api_mode"]' => [
            'value' => 'live',
          ],
        ],
      ],
    ];

    $form['cdek_api_password'] = [
      '#type' => 'textfield',
      '#title' => $this->t('CDEK API account password'),
      '#default_value' => $this->config('cdek_api_wrapper.settings')->get('cdek_api_password'),
      '#states' => [
        'visible' => [
          ':input[name="cdek_api_mode"]' => [
            'value' => 'live',
          ],
        ],
      ],
    ];

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state): void {
    $this->config('cdek_api_wrapper.settings')
      ->set('cdek_api_mode', $form_state->getValue('cdek_api_mode'))
      ->set('cdek_api_login', $form_state->getValue('cdek_api_login'))
      ->set('cdek_api_password', $form_state->getValue('cdek_api_password'))
      ->save();

    parent::submitForm($form, $form_state);
  }

}
