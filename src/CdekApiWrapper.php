<?php

namespace Drupal\cdek_api_wrapper;

use AntistressStore\CdekSDK2\CdekClientV2 as CdekClient;

class CdekApiWrapper {

  public CdekClient $cdekClient;

  /**
   * Class constructor.
   */
  public function __construct() {
    $cdek_api_wrapper_config = \Drupal::config('cdek_api_wrapper.settings'); // @TODO Use DI

    if ($cdek_api_wrapper_config->get('cdek_api_mode') == 'test') {
      $this->cdekClient = new CdekClient('TEST');
    }
    else {
      $this->cdekClient = new CdekClient($cdek_api_wrapper_config->get('cdek_api_login'), $cdek_api_wrapper_config->get('cdek_api_password'));
    }
  }

}
